import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reserve',
  templateUrl: './reserve.component.html',
  styleUrls: ['./reserve.component.scss']
})
export class ReserveComponent implements OnInit {

  name: any = '';
  address: any = '';
  email: any = '';
  city: any = '';
  select: any = '';
  size: any = '';
  rate: any = '';
  period: any = 'Monthly';

  breadcrumbActive: any = 'Reserve Your Unit';
  step = 1;
  head: any = 'Rate Chart';

  list: any = [
    {
      data: 'Outdoor Storage',
      rate: '$35.00'
    },
    {
      data: '05 x 05 CC',
      rate: '$59.95'
    },
    {
      data: '10 x 10',
      rate: '$87.95'
    },
    {
      data: '10 x 15',
      rate: '$124.95'
    },
    {
      data: '10 x 10 CC',
      rate: '$139.95'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

  public formChange(step) {
    if (this.name !== '' && this.email !== '' && this.city !== '') {
      this.step += step;
    } else {
      alert('Please fill out all the required fields');
    }
  }

  onChange(size) {
    this.size = size.split(': ')[1];
    let value: any;
    for (value of this.list) {
      if (value.data === this.size) {
        this.rate = value.rate;
      }
    }
  }


  public validate(check, value, id, helpId) {
    console.log(check + ' ' + value);
    if (check === 'notNull') {
      if (this.validateNull(value)) {
        document.getElementById(id).style.borderColor = 'red';
        document.getElementById(helpId).innerHTML = 'Please fill out this field';
      } else {
        document.getElementById(id).style.border = '1px solid #ced4da';
        document.getElementById(helpId).innerHTML = '';
      }
    }
    if (check === 'email') {
      if (this.validateNull(value)) {
        document.getElementById(id).style.borderColor = 'red';
        document.getElementById(helpId).innerHTML = 'Please fill out this field';
      } else {
        if (!this.validateEmail(value)) {
          document.getElementById(id).style.borderColor = 'red';
          document.getElementById(helpId).innerHTML = 'Please enter a valid email id';
        } else {
          document.getElementById(id).style.border = '1px solid #ced4da';
          document.getElementById(helpId).innerHTML = '';
        }
      }
    }
  }

  private validateEmail(value) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
      return (true);
    }
    return (false);
  }

  private validateNull(value) {
    if (value === undefined || value === '') {
      return (true);
    }
    return (false);
  }

}
