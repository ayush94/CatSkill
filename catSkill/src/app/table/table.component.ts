import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  th: any;
  tds: String;
  objectKeys: any;
  @Input('tBody') tBody;
  @Input('tHead') tHead;
  constructor() { }

  ngOnInit() {
    this.th = this.tHead;
    this.tds = this.tBody;
    this.objectKeys = Object.keys(this.tds[0]);
  }

}
