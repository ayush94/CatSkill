import { Component, OnInit } from '@angular/core';
import { RouterLink, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  head: any = 'The Only Self Storage in Greene County With All of The Following';
  list: any = [
    {
      data1: 'Fully Lit Facility',
      data2: 'Climate Controlled Units'
    },
    {
      data1: 'Fenced In Facility',
      data2: '24 Hour Video Security'
    },
    {
      data1: 'Gated Access',
      data2: 'Superior Customer Service'
    },
    {
      data1: 'Black Top Paving',
      data2: 'U-Haul Rentals'
    },
    {
      data1: 'Packing Material',
      data2: 'Tenant Insurance'
    }
  ];

  hours: any = [
    {
      label: 'Mon-Fri:',
      data: '9:00 am - 6:00 pm'
    },
    {
      label: 'Sat:',
      data: '9:00 am - 3:00 pm'
    },
    {
      label: 'sunday:',
      data: '8:00 am - 12:00 pm'
    },
    {
      label: 'Facility Access With Pin Code:',
      data: '24 hours a day, 7 days a week'
    }
  ];

  contact: any = [
    {
      label: 'Phone:',
      data: '518-943 - 3003'
    },
    {
      label: 'Email:',
      data: 'catskill@gmail.com'
    },
    {
      label: 'Address:',
      data: '5877 Cauterskill Road Catskill, NY 12414'
    }
  ];

  public navigate(location) {
    this.router.navigate([location]);
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
  }

}
