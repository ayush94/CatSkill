import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})

export class ContactComponent implements OnInit {

  breadcrumbActive: any = 'Contact Us';
  name: any = '';
  hours: any = [
    {
      label: 'Mon-Fri:',
      data: '9:00 am - 6:00 pm'
    },
    {
      label: 'Sat:',
      data: '9:00 am - 3:00 pm'
    },
    {
      label: 'sunday:',
      data: '8:00 am - 12:00 pm'
    },
    {
      label: 'Facility Access With Pin Code:',
      data: '24 hours a day, 7 days a week'
    }
  ];

  contact: any = [
    {
      label: 'Phone:',
      data: '518-943 - 3003'
    },
    {
      label: 'Email:',
      data: 'catskillselfstorage@gmail.com'
    },
    {
      label: 'Address:',
      data: '5877 Cauterskill Road Catskill, NY 12414'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
